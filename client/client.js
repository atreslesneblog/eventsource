var template = '<i class="comment icon"></i><div class="content"><div class="header">USERNAME <span class="ui tag mini label">DATE</span></div><div class="description">MESSAGE</div></div>';

function ChatClient(subscribeUrl, sendUrl) {
    if (!window.EventSource) {
        console.error('EventSource not supported');
        return;
    }

    this.form = document.forms['sendform'];
    this.urls = {
        subscribe: subscribeUrl,
        send: sendUrl
    };
    this.messages = document.getElementById('messages');
    this.container = document.getElementById('container');

    this.form.onsubmit = function () {
        if (!/\bdisabled\b/.test(this.form.button.className)) {
            this.send(this.form.message.value, this.form.author.value);
        }
        return false;
    }.bind(this);

    this.form.message.onkeyup = function () {
        if (this.form.message.value && /\bdisabled\b/.test(this.form.button.className)) {
            this.form.button.className = this.form.button.className.replace(/disabled/, '');
        } else if (!this.form.message.value && !/\bdisabled\b/.test(this.form.button.className)) {
            this.form.button.className += ' disabled';
        }
    }.bind(this);

    this.subscribe();
}

ChatClient.prototype.send = function (message, author) {
    this.form.message.value = '';
    this.form.button.className += ' disabled';

    var xhr = new XMLHttpRequest();
    xhr.open("POST", this.urls.send, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        message: message,
        author: author || 'Anonymous'
    }));
};

ChatClient.prototype.subscribe = function () {
    this.es = new EventSource(this.urls.subscribe);

    this.es.onerror = function () {
        if (!/\bnegative\b/.test(this.form.button.className)) {
            this.form.button.className += ' negative';
        }
    }.bind(this);

    this.es.onopen = function () {
        if (/\bnegative\b/.test(this.form.button.className)) {
            this.form.button.className = this.form.button.className.replace(/negative/, '').trim();
        }
    }.bind(this);

    this.es.onmessage = function (event) {
        var data = JSON.parse(event.data);
        this.displayMessage(data.message, data.author);
    }.bind(this);
};

ChatClient.prototype.dateFormat = function (date) {
    function prependZero(value) {
        return (parseInt(value) < 10) ? '0' + value : value;
    }
    return [date.getHours(), date.getMinutes(), date.getSeconds()].map(function (v) {
        return prependZero(v)
    }).join(':');
};

ChatClient.prototype.displayMessage = function (message, author) {
    var el = document.createElement('div');
    el.className = 'item';
    el.innerHTML = template.replace(/USERNAME/, author).replace(/DATE/, this.dateFormat(new Date)).replace(/MESSAGE/, message);
    this.messages.appendChild(el);
    this.container.scrollTop = this.container.scrollHeight;
};
