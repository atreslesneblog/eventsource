'use strict';

const express = require('express');
const parser = require('body-parser');
const events = require('events');

const dispatcher = new events.EventEmitter();
const app = express();

app.post('/send', parser.json(), (req, res) => {
    dispatcher.emit('message', JSON.stringify(req.body));
    res.set('Content-Type', 'text/plain;charset=utf-8');
    res.end('ok');
});

app.get('/subscribe', (req, res) => {
    res.status(200);
    res.set('Content-Type', 'text/event-stream;charset=utf-8');
    res.set('Cache-Control', 'no-cache');
    res.write(': open stream\n\n');

    let fn = (message) => res.write(`data: ${message}\n\n`);

    dispatcher.on('message', fn);
    req.connection.on('close', () => dispatcher.removeListener('message', fn));
});

app.use(express.static('../../client'));
app.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
