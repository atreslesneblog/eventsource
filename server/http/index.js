'use strict';

const http = require('http');
const url = require('url');
const serve = require('node-static');
const events = require('events');

const dispatcher = new events.EventEmitter();
const statics = new serve.Server('../../client');

const server = http.createServer((req, res) => {
    let parsed = url.parse(req.url, true);

    if (parsed.pathname == '/subscribe') {
        var fn = (message) => res.write(`data: ${message}\n\n`);

        res.writeHead(200, {
            'Content-Type': 'text/event-stream;charset=utf-8',
            'Cache-Control': 'no-cache'
        });
        res.write(': open stream\n\n');

        dispatcher.on('message', fn);
        req.connection.on('close', () => dispatcher.removeListener('message', fn));
        return;
    }

    if (parsed.pathname == '/send' && req.method == 'POST') {
        req.setEncoding('utf8');
        res.setHeader('Content-Type', 'text/plain;charset=utf-8');
        let message = '';
        req.on('data', chunk => message += chunk).on('end', () => {
            dispatcher.emit('message', message);
            res.end('ok');
        });
        return;
    }

    statics.serve(req, res);
});

server.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
