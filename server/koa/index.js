'use strict';

const koa = require('koa');
const router = require('koa-router')();
const parser = require('koa-bodyparser');
const serve = require('koa-static');
const events = require('events');

const PassThrough = require('stream').PassThrough;
const dispatcher = new events.EventEmitter();
const app = koa();

router.post('/send', parser(), function *() {
    dispatcher.emit('message', this.request.body);
    this.response.set('Content-Type', 'text/plain;charset=utf-8');
    this.body = 'ok';
});

router.get('/subscribe', function *() {
    let stream = new PassThrough();

    let fn = (message) => stream.write(`data: ${JSON.stringify(message)}\n\n`);
    let finish = () => dispatcher.removeListener('message', fn);

    this.response.status = 200;
    this.response.type = 'text/event-stream;charset=utf-8';
    this.response.set('Cache-Control', 'no-cache');
    this.body = stream;

    stream.write(': open stream\n\n');
    dispatcher.on('message', fn);
    this.req.on('close', finish);
    this.req.on('finish', finish);
    this.req.on('error', finish);
});

app.use(router.routes()).use(router.allowedMethods()).use(serve('../../client'));
app.listen(process.env.PORT || 8080, process.env.IP || '0.0.0.0');
